package base;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.net.MalformedURLException;

public class AppDriver {
    //Getter
    public static WebDriver getDriver(){
        return driver.get();
    }

    //Setter
    static void setDriver(WebDriver Driver){
        driver.set(Driver);
    }

    private static ThreadLocal<WebDriver> driver = new ThreadLocal<>();


}
