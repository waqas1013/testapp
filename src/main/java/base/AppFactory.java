package base;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class AppFactory {
    public static AppiumDriver<MobileElement> driver;
    public static DesiredCapabilities capabilities;

    public static void iOSApp_Launch() throws MalformedURLException {
        capabilities = new DesiredCapabilities();
        capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", "iPhone 11");
        capabilities.setCapability("platformName", "iOS" );
        capabilities.setCapability("platformVersion", "14.3" );
        capabilities.setCapability("automationName", "XCUITest");
        capabilities.setCapability("app", "/Users/muhammadwaqassarwar/Desktop/TestApp.app");

        driver = new IOSDriver<MobileElement>(new URL("http://0.0.0.0:4723/wd/hub"),capabilities);
        AppDriver.setDriver(driver);
    }

    public static void androidApp_Launch() throws MalformedURLException {
        capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", "emulator-5554");
        capabilities.setCapability("platformName", "Android" );
        capabilities.setCapability("platformVersion", "10.0" );
        capabilities.setCapability("app", "/Users/muhammadwaqassarwar/Desktop/selendroid-test-app-0.17.0.apk");
        capabilities.setCapability("appActivity","io.selendroid.testapp.HomeScreenActivity");
        capabilities.setCapability("appPackage", "io.selendroid.testapp");

        driver = new AndroidDriver<MobileElement>(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
        AppDriver.setDriver(driver);
    }

   public static void closeApp(){
        driver.quit();
    }


}
