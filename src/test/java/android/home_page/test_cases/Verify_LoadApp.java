package android.home_page.test_cases;

import android.favorites.page_object.HomeScreen;
import base.AppFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;


public class Verify_LoadApp {

    @Test(priority = 1)
    public void testCase_goToApp() throws InterruptedException {
        HomeScreen homeScreen = new HomeScreen();
        homeScreen.preScreensToStartApp();
    }

    @Test(priority = 2)
    public void testCase_TitleAndDescription() throws InterruptedException {
        HomeScreen homeScreen = new HomeScreen();
        homeScreen.preScreensToStartApp();
        Thread.sleep(2000);
        homeScreen.checkTitleAndDescriptionExists();
    }

    @Test(priority = 3)
    public void testCase_TapOnButton() throws InterruptedException {
        HomeScreen homeScreen = new HomeScreen();
        homeScreen.preScreensToStartApp();
        homeScreen.tapOnButton();
    }

}