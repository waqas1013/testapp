package android.home_page.page_object;

import base.AppDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;

public class HomeScreen {
    public HomeScreen(){
        PageFactory.initElements(new AppiumFieldDecorator(AppDriver.getDriver()), this);
    }

    @FindBy(xpath = "\t\n" +
            "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.Button[2]")
    public WebElement continueButton;
    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button")
    public WebElement okButton;
    @FindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.widget.TextView")
    public WebElement title;
    @FindBy(xpath = "//android.widget.LinearLayout[@content-desc=\"l10nCD\"]/android.widget.TextView")
    public WebElement description;
    @FindBy(xpath = "//android.widget.Button[@content-desc=\"buttonTestCD\"]")
    public WebElement enButton;
    @FindBy(id = "message")
    public WebElement dialogueMessage;
    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.Button[2]")
    public WebElement selectNoFromDialogueBox;


    public void preScreensToStartApp() throws InterruptedException {

        Thread.sleep(5000);
        continueButton.click();
        Thread.sleep(5000);
        okButton.click();
    }

    public void checkTitleAndDescriptionExists(){
        Assert.assertEquals("Hello Default Locale, Selendroid-test-app!", title.getText(), "Title is not matched");
        Assert.assertEquals("Localization (L10n) locator test", description.getText(), "Description is not matched");

        /*if (title.getText().matches("Hello Default Locale, Selendroid-test-app!")){
            System.out.println("Test Pass");
        }else {
            System.out.println("Test Fail");
        }*/
    }

    public void tapOnButton(){
        enButton.click();
    }

    public void checkDialogueBox(){
        Assert.assertEquals("This will end the activity", dialogueMessage.getText(), "Text Not matched");
        selectNoFromDialogueBox.click();

    }

}
