package android.favorites.test_cases;

import android.favorites.page_object.HomeScreen;
import org.testng.annotations.Test;

public class Verify_TitleAndDescription {
    HomeScreen homeScreen = new HomeScreen();

    @Test()
    public void testCase_TitleAndDescription(){
        homeScreen.checkTitleAndDescriptionExists();
    }

    @Test
    public void testCase_TapOnButton(){
        homeScreen.tapOnButton();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
