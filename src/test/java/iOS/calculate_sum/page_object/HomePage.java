package iOS.calculate_sum.page_object;

import base.AppDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class HomePage {
    //create constructor..this constructor will hold the page factory

    public HomePage(){
        PageFactory.initElements(new AppiumFieldDecorator(AppDriver.getDriver()), this);
    }

    @FindBy(id = "IntegerA")
    public WebElement firstField;
    @FindBy(id = "IntegerB")
    public WebElement secondField;
    @FindBy(id = "Compute Sum")
    public WebElement sumButton;
    @FindBy(id = "Answer")
    public WebElement answer;

    public void calculateAndCheckAnswer(){
        firstField.clear();
        firstField.sendKeys("8");
        secondField.clear();
        secondField.sendKeys("2");
        sumButton.click();

       int first = Integer.parseInt(firstField.getText());
       int second = Integer.parseInt(secondField.getText());
       String sum = String.valueOf(first+second);
       Assert.assertEquals(sum,answer.getText(),"Answer do not match");
    }

}
